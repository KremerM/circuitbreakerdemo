package circuitbreaker.client;

public class Response {
	
	private Status statusCode;
	private String message;
	
	public Response(Status statusCode, String message) {
		super();
		this.statusCode = statusCode;
		this.message = message;
	}
	
	public Status getStatusCode() {
		return statusCode;
	}
	public String getMessage() {
		return message;
	}
	
}
