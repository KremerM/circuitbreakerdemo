package circuitbreaker.client.slow;

import circuitbreaker.client.Client;
import circuitbreaker.client.Response;
import circuitbreaker.client.Status;
import com.netflix.hystrix.*;

public class HystrixSlowClient extends HystrixCommand<Response> implements Client {

    private Client slowClient;

    public HystrixSlowClient(int secondsToWaitForResponse) {
        super(createConfig());
        this.slowClient = new SlowClient(secondsToWaitForResponse);
    }

    private static Setter createConfig(){
        Setter config = HystrixCommand
                .Setter
                .withGroupKey(HystrixCommandGroupKey.Factory.asKey("RemoteServiceGroupTest5"));

        HystrixCommandProperties.Setter commandProperties = HystrixCommandProperties.Setter();
        commandProperties.withExecutionTimeoutInMilliseconds(2_000);

        config.andCommandPropertiesDefaults(commandProperties);

        return config;
    }

    @Override
    public Response run() {
        return slowClient.send();
    }

    @Override
    public Response getFallback() {
        return new Response(Status.OK, "fallback");
    }

    @Override
    public Response send() {
        return execute();
    }

}
