package circuitbreaker.client.slow;

import java.time.Duration;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.function.Supplier;

import circuitbreaker.client.Client;
import circuitbreaker.client.Response;
import circuitbreaker.client.Status;
import io.github.resilience4j.timelimiter.TimeLimiter;
import io.github.resilience4j.timelimiter.TimeLimiterConfig;
import io.vavr.control.Try;

public class Resilience4JSlowClient implements Client {

	private SlowClient slowClient;
	
	public Resilience4JSlowClient(int secondsToWaitForResponse) {
		slowClient = new SlowClient(secondsToWaitForResponse);
	}

	public Response send() {
		TimeLimiterConfig config = TimeLimiterConfig.custom().timeoutDuration(Duration.ofSeconds(2))
				.cancelRunningFuture(true).build();

		TimeLimiter timeLimiter = TimeLimiter.of(config);
		ExecutorService executorService = Executors.newSingleThreadExecutor();
		Supplier<Future<Response>> futureSupplier = () -> executorService.submit(() -> slowClient.send());

		Callable<Response> restrictedCall = TimeLimiter.decorateFutureSupplier(timeLimiter, futureSupplier);

		Response response = Try.ofCallable(restrictedCall).onFailure((throwable) -> System.out.println(throwable))
				.recover((thowable) -> new Response(Status.OK, "fallback")).get();

		return response;
	}

}
