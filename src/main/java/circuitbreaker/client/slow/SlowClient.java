package circuitbreaker.client.slow;

import circuitbreaker.client.Client;
import circuitbreaker.client.DefaultClient;
import circuitbreaker.client.Response;

public class SlowClient extends DefaultClient implements Client {
	
	private int seconds;
	
	public SlowClient(int secondsToWaitForResponse) {
		this.seconds = secondsToWaitForResponse;
	}
	
	@Override
	public Response send() {
		try {
			Thread.sleep(seconds * 1000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		return super.send();
	}

}
