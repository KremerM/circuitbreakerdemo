package circuitbreaker.client.slow;

import circuitbreaker.client.Client;
import circuitbreaker.client.Response;
import circuitbreaker.client.Status;
import com.alibaba.csp.sentinel.Entry;
import com.alibaba.csp.sentinel.SphU;
import com.alibaba.csp.sentinel.slots.block.BlockException;
import com.alibaba.csp.sentinel.slots.block.RuleConstant;
import com.alibaba.csp.sentinel.slots.block.degrade.DegradeRule;
import com.alibaba.csp.sentinel.slots.block.degrade.DegradeRuleManager;

import java.util.ArrayList;
import java.util.List;

public class SentinelSlowClient implements Client {

    private SlowClient slowClient;

    public SentinelSlowClient(int secondsToWaitForResponse) {
        this.slowClient = new SlowClient(secondsToWaitForResponse);

        initDegradeRule();
    }

    private static void initDegradeRule() {
        List<DegradeRule> rules = new ArrayList<DegradeRule>();
        DegradeRule rule = new DegradeRule();
        rule.setResource("resourceName");
        rule.setCount(1000);
        rule.setGrade(RuleConstant.DEGRADE_GRADE_RT);
        rule.setTimeWindow(10);
        rules.add(rule);
        DegradeRuleManager.loadRules(rules);
    }

    @Override
    public Response send() {
        Entry entry = null;
        try {
            entry = SphU.entry("resourceName");
            return slowClient.send();
        } catch (BlockException e) {
            return new Response(Status.OK, "fallback");
        } finally {
            if (entry != null) {
                entry.exit();
            }
        }

    }
    }
