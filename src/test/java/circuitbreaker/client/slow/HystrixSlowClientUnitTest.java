package circuitbreaker.client.slow;

import circuitbreaker.client.Client;
import circuitbreaker.client.Response;
import circuitbreaker.client.Status;
import org.junit.jupiter.api.Test;

import java.time.Duration;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertTimeout;

public class HystrixSlowClientUnitTest {

    @Test
    public void sendDurationIsBelow3Seconds_Hystrix() {
        Client classUnderTest = new HystrixSlowClient(5);

        assertTimeout(Duration.ofSeconds(3), () -> {
            classUnderTest.send();
        });
    }

    @Test
    public void sendReturnsDefaultResponse_whenResponseTakesTooLong_Hystrix() {
        Client classUnderTest = new HystrixSlowClient(5);

        Response response = classUnderTest.send();

        assertThat(response.getStatusCode()).isEqualTo(Status.OK);
        assertThat(response.getMessage()).isEqualTo("fallback");
    }
}
