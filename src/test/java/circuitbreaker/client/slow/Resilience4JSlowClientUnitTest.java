package circuitbreaker.client.slow;

import circuitbreaker.client.Client;
import circuitbreaker.client.Response;
import circuitbreaker.client.Status;
import org.junit.jupiter.api.Test;

import java.time.Duration;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertTimeout;

public class Resilience4JSlowClientUnitTest {

    @Test
    public void sendDurationIsBelow3Seconds_resilient4j() {
        Client classUnderTest = new Resilience4JSlowClient(5);

        assertTimeout(Duration.ofSeconds(3), () -> {
            classUnderTest.send();
        });
    }

    @Test
    public void sendReturnsDefaultResponse_whenResponseTakesTooLong_resilient4j() {
        Client classUnderTest = new Resilience4JSlowClient(3);

        Response response = classUnderTest.send();

        assertThat(response.getStatusCode()).isEqualTo(Status.OK);
        assertThat(response.getMessage()).isEqualTo("fallback");
    }
}
