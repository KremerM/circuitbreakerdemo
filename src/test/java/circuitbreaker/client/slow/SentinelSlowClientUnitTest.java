package circuitbreaker.client.slow;

import circuitbreaker.client.Response;
import circuitbreaker.client.Status;
import org.junit.jupiter.api.Test;

import java.time.Duration;
import java.util.stream.IntStream;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertTimeout;

public class SentinelSlowClientUnitTest {

    @Test
    public void sendDurationIsBelow3Seconds() {
        SentinelSlowClient classUnderTest = new SentinelSlowClient(1);

        assertTimeout(Duration.ofSeconds(6), () -> {
            IntStream.rangeClosed(1, 6).forEach((i) -> classUnderTest.send());
        });
    }

    @Test
    public void sendReturnsDefaultResponse_whenResponseTakesTooLong() {
        SentinelSlowClient classUnderTest = new SentinelSlowClient(1);

        IntStream.rangeClosed(1, 5).forEach((i) -> classUnderTest.send());
        Response response = classUnderTest.send();

        assertThat(response.getStatusCode()).isEqualTo(Status.OK);
        assertThat(response.getMessage()).isEqualTo("fallback");
    }
}
